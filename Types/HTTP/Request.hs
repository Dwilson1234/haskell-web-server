-- |This module contains all the types pertaining to HTTP Requests
module Types.HTTP.Request where

import Types.HTTP.Headers
import Types.HTTP.Misc
import qualified Data.ByteString.Char8 as B

-- |The types for a Request Line
data HTTPRequest =
    GET URIProt Headers |
    POST URIProt Headers (Maybe Message)

instance Show HTTPRequest where
    show (GET up hs) = "GET" ++ (' ':show up) ++ (' ':show hs)
    show (POST up hs (Just m)) = "POST" ++ (' ':show up) ++ (' ':show hs) ++ (' ':show m)
    show (POST up hs Nothing) = "POST" ++ (' ':show up)  ++ (' ':show hs)

data URIProt = URIProt URI Protocol

instance Show URIProt where
    show (URIProt uri p) = show uri ++ (' ':show p)

getRequestLine (GET up _ ) = "GET" ++ (' ':show up)
getRequestLine (POST up _ _ ) = "POST" ++ (' ':show up)

getURI (GET (URIProt uri _) _) = uri
getURI (POST (URIProt uri _) _ _) = uri

getProtocol (GET (URIProt _ p) _) = p
getProtocol (POST (URIProt _ p) _ _) = p

getReqMessage (POST _ _ (Just m)) = m
getReqMessage (POST _ _ Nothing ) = Message ""
getReqMessage _ = Message ""

getReqHeaders (GET _ hs) = hs
getReqHeaders (POST _ hs _ ) = hs

insertMessage (POST up hs _) m = POST up hs (Just m)
insertMessage req _ = req

