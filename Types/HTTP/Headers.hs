-- |This module contains all the header types required for HTTP.
module Types.HTTP.Headers where

import qualified Data.ByteString.Char8 as B

-- |The header types
newtype HValue = HValue B.ByteString
instance Show HValue where
    show (HValue v) = B.unpack v
data Header = RequestHeader RHName HValue | GeneralHeader GHName HValue | EntityHeader EHName HValue | CustomHeader B.ByteString HValue | ResponseHeader RespName HValue
instance Show Header where
    show (RequestHeader n v) = (show n) ++ ": " ++ (show v)
    show (GeneralHeader n v) = (show n) ++ ": " ++ (show v)
    show (EntityHeader n v) = (show n) ++ ": " ++ (show v)
    show (CustomHeader n v) = (show n) ++ ": " ++ (show v)
    show (ResponseHeader n v) = (show n) ++ ": " ++ (show v)
type Headers = [Header]

data RHName = Accept | AcceptCharset | AcceptEncoding | AcceptLanguage | Authorization | Expect |
        From | Host | IfMatch | IfModifiedSince | IfNoneMatch | IfRange | IfUnmodifiedSince | MaxForwards |
        ProxyAuthorization | Range | Referer | TE | UserAgent
instance Show RHName where
    show Accept = "Accept"
    show AcceptCharset = "Accept-Charset"
    show AcceptEncoding = "Accept-Encoding"
    show AcceptLanguage = "Accept-Language"
    show Authorization = "Authorization"
    show Expect = "Expect"
    show From = "From"
    show Host = "Host"
    show IfMatch = "If-Match"
    show IfModifiedSince = "If-Modified-Since"
    show IfNoneMatch = "If-None-Match"
    show IfRange = "If-Range"
    show IfUnmodifiedSince = "If-Unmodified-Since"
    show MaxForwards = "Max-Forwards"
    show ProxyAuthorization = "Proxy-Authorization"
    show Range = "Range"
    show Referer = "Referer"
    show TE = "TE"
    show UserAgent = "User-Agent"

data GHName = CacheControl | Connection | Date | Pragma | Trailer | TransferEncoding | Upgrade | Via | Warning
instance Show GHName where
    show CacheControl = "Cache-Control"
    show Connection = "Connection"
    show Date = "Date"
    show Pragma = "Pragma"
    show Trailer = "Trailer"
    show TransferEncoding = "Transfer-Encoding"
    show Upgrade = "Upgrade"
    show Via = "Via"
    show Warning = "Warning"

data EHName = Allow | ContentEncoding | ContentLanguage | ContentLength | ContentLocation | ContentMD5 | ContentRange |
        ContentType | Expires | LastModified
instance Show EHName where
    show Allow = "Allow"
    show ContentEncoding = "Content-Encoding"
    show ContentLanguage = "Content-Language"
    show ContentLength = "Content-Length"
    show ContentLocation = "Content-Location"
    show ContentMD5 = "Content-MD5"
    show ContentRange = "Content-Range"
    show ContentType = "Content-Type"
    show Expires = "Expires"
    show LastModified = "Last-Modified"

data RespName = AcceptRanges | Age | ETag | Location | ProxyAuthenticate | RetryAfter | Server | Vary | WWWAuthenticate
instance Show RespName where
    show AcceptRanges = "Accept-Ranges"
    show Age = "Age"
    show ETag = "ETag"
    show Location = "Location"
    show ProxyAuthenticate = "Proxy-Authenticate"
    show RetryAfter = "Retry-After"
    show Server = "Server"
    show Vary = "Vary"
    show WWWAuthenticate = "WWW-Authenticate"

getHost ((RequestHeader Host (HValue h)):hs) = Just (show h)
getHost (h:[]) = Nothing
getHost (h:hs) = getHost hs

getDate ((GeneralHeader Date (HValue d)):hs) = Just (show d)
getDate (h:[]) = Nothing
getDate (h:hs) = getDate hs

