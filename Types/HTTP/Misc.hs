-- |This module contains types used in multiple HTTP contexts.
module Types.HTTP.Misc where

import qualified Data.ByteString.Char8 as B

newtype Message = Message String
instance Show Message where
    show (Message m) = m

newtype URI = URI B.ByteString

instance Show URI where
    show (URI uri) = show uri

type MajorVersion = Integer
type MinorVersion = Integer
data Protocol = Protocol MajorVersion MinorVersion
instance Show Protocol where
    show (Protocol maj min) = "HTTP/" ++ (show maj) ++ "." ++ (show min)
