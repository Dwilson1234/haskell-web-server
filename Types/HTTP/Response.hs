-- |This module contains all the types pertaining to HTTP response bodies.
module Types.HTTP.Response where

import Types.HTTP.Headers
import Types.HTTP.Misc
import Control.Proxy
import Data.List

data StatusLine = StatusLine Protocol StatusCode
instance Show StatusLine where
    show (StatusLine p sc) = (show p) ++ " " ++ (show sc)

data HTTPResponse = HTTPResponse Message Headers StatusLine
instance Show HTTPResponse where
    show (HTTPResponse m hs sl) = (show sl) ++ "\r\n" ++ (concat (intersperse "\r\n" (map show hs))) ++ "\r\n\r\n" ++ (show m)

type Code = Integer
type Reason = String

data StatusCode = StatusCode Code Reason
instance Show StatusCode where
    show (StatusCode c r) = (show c) ++ " " ++ (show r)

-- * Various prebuilt status codes and pages.
mkStatusLine = StatusLine (Protocol 1 1)
sc200 = StatusCode 200 "OK"
sc404 = StatusCode 404 "Not Found"
notfound = "<html><head></head><body>404 not found</body></html>"
sc500 = StatusCode 500 "Internal Error"
internalerror = "<html><head></head><body>500 Internal Error</body></html>"
sc501 = StatusCode 501 "Not Implemented"

getStatusCode (HTTPResponse _ _ (StatusLine _ (StatusCode i _))) = i
getRespMessage (HTTPResponse m _ _) = m
getRespHeaders (HTTPResponse _ hs _) = hs
