{-# LANGUAGE DeriveDataTypeable #-}
-- |Exception types. This module exists mostly so I can flatten ExceptionP Pipes into a single layer.
module Types.Exceptions where

import Control.Exception
import Control.Exception.Base
import Control.Proxy.Attoparsec.Types
import Data.Typeable

-- |Exception for 'Attoparsec' parsers. So it can be part of 'SomeException'.
data ParserException = ParserException BadInput
    deriving (Typeable)

instance Show ParserException where
    show (ParserException (MalformedInput (ParserError ecs em))) =
        "ParserException (MalformedInput): " ++ em ++ "\n\tContexts: " ++ (show ecs)
    show (ParserException (InputTooLong l)) = "ParserException (InputTooLong), input length: " ++ (show l)

instance Exception ParserException

