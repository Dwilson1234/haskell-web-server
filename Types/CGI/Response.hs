-- |This module contains types for CGI specific response messages.
module Types.CGI.Response where

import Types.CGI.Headers
import Types.CGI.Environment
import Types.HTTP.Response
import qualified Types.HTTP.Headers as H
import Types.HTTP.Misc
import Data.ByteString.Char8 as B

data CGIResponse = CGIResponse CGIHeaders B.ByteString
    deriving (Show)

-- |Converts a CGI response into parts of an 'HTTPResponse'. Threads the given 'HTTPRequest' through the function for later use.
cgi2httpparts (httpreq, (CGIResponse hs s)) = (httpreq, ((B.unpack s), doHeaders hs, (StatusCode (getCGIStatus hs) "")))
    where
        doHeaders [] = []
        doHeaders ((ContentType v):hs) = (H.EntityHeader H.ContentType (H.HValue v)):(doHeaders hs)
        doHeaders (h:hs) = doHeaders hs
