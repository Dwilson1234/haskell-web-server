-- |This module contains types for CGI specific headers.
module Types.CGI.Headers where

import qualified Data.ByteString.Char8 as B

data CGIHeader = ContentType B.ByteString | Location B.ByteString | Status Integer | CustomHeader B.ByteString
    deriving (Show)

type CGIHeaders = [CGIHeader]

getCGIStatus ((Status i):_) = i
getCGIStatus (_:[]) = 201
getCGIStatus (_:xs) = getCGIStatus xs
