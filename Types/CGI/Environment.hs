-- |This module contains types for all the environment variables needed for
-- CGI implementation.
module Types.CGI.Environment where

import qualified Types.HTTP.Headers as H
import Types.HTTP.Request
import Types.HTTP.Misc
import Data.Char
import qualified Data.ByteString.Char8 as B
import Config

getEnvVs req = fromHeaders (getReqHeaders req) ++ fromReq req ++ defaultEnvs
    where
        fromHeaders [] = []
        fromHeaders ((H.EntityHeader H.ContentType (H.HValue v)):hs) = (CONTENT_TYPE (B.unpack v)):fromHeaders hs
        fromHeaders ((H.EntityHeader H.ContentLength (H.HValue v)):hs) =
            case B.readInteger v of
                Nothing -> fromHeaders hs
                Just (i,_) -> (CONTENT_LENGTH i):fromHeaders hs
        fromHeaders (_:hs) = fromHeaders hs
        fromReq (GET _ _) = [REQUEST_METHOD "GET"]
        fromReq (POST (URIProt (URI i) _) _ _) = [REQUEST_METHOD "POST", PATH_INFO (documentRoot ++ B.unpack i), SCRIPT_NAME (documentRoot ++ B.unpack i)]

defaultEnvs = [
    GATEWAY_INTERFACE "CGI/1.1"
    ]

data EnvV =
    AUTH_TYPE String |
    CONTENT_LENGTH Integer |
    CONTENT_TYPE String |
    GATEWAY_INTERFACE String |
    HTTP H.Header |
    PATH_INFO String |
    PATH_TRANSLATED String |
    QUERY_STRING String |
    REMOTE_ADDR String |
    REMOTE_HOST String |
    REMOTE_IDENT String |
    REMOTE_USER String |
    REQUEST_METHOD String |
    SCRIPT_NAME String |
    SERVER_NAME String |
    SERVER_PORT String |
    SERVER_PROTOCOL String |
    SERVER_SOFTWARE String |
    REDIRECT_STATUS String

instance Show EnvV where
    show (AUTH_TYPE v) = "AUTH_TYPE=" ++ v
    show (CONTENT_LENGTH v) = "CONTENT_LENGTH=" ++ show v
    show (CONTENT_TYPE v) = "CONTENT_TYPE=" ++ v
    show (GATEWAY_INTERFACE v) = "GATEWAY_INTERFACE=" ++ v
    show (HTTP (H.RequestHeader n v)) = "HTTP_" ++ map (\x -> if x=='-' then '_' else x) (map toUpper (show n)) ++ "=" ++ show v
    show (HTTP (H.EntityHeader n v)) = "HTTP_" ++ map (\x -> if x=='-' then '_' else x) (map toUpper (show n)) ++ "=" ++ show v
    show (HTTP (H.GeneralHeader n v)) = "HTTP_" ++ map (\x -> if x=='-' then '_' else x) (map toUpper (show n)) ++ "=" ++ show v
    show (HTTP (H.CustomHeader n v)) = "HTTP_" ++ map (\x -> if x=='-' then '_' else x) (map toUpper (show n)) ++ "=" ++ show v
    show (PATH_INFO v) = "PATH_INFO=" ++ v
    show (PATH_TRANSLATED v) = "PATH_TRANSLATED=" ++ v
    show (QUERY_STRING v) = "QUERY_STRING=" ++ v
    show (REMOTE_ADDR v) = "REMOTE_ADDR=" ++ v
    show (REMOTE_HOST v) = "REMOTE_HOST=" ++ v
    show (REMOTE_IDENT v) = "REMOTE_IDENT=" ++ v
    show (REMOTE_USER v) = "REMOTE_USER=" ++ v
    show (REQUEST_METHOD v) = "REQUEST_METHOD=" ++ v
    show (SCRIPT_NAME v) = "SCRIPT_NAME=" ++ v
    show (SERVER_NAME v) = "SERVER_NAME=" ++ v
    show (SERVER_PORT v) = "SERVER_PORT=" ++ v
    show (SERVER_PROTOCOL v) = "SERVER_PROTOCOL=" ++ v
    show (SERVER_SOFTWARE v) = "SERVER_SOFTWARE=" ++ v
    show (REDIRECT_STATUS v) = "REDIRECT_STATUS=" ++ v
    
