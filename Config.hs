-- |This module contains configuration information for the server
module Config where

-- |The location of the document root relative to the server's binary
documentRoot = "www"

-- |The server host addresses to bind to. Set to * to bind to all available addresses.
serverHost = "127.0.0.1"

-- |The server port to bind to.
serverPort = "8080"
