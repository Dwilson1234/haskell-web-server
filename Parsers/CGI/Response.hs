-- |This module contains all the parsers required to parse a CGI response value.
module Parsers.CGI.Response (parseCGIProxy) where

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.Attoparsec.Combinator as AC
import Data.Attoparsec.ByteString.Char8 (Parser)
import Data.Attoparsec.ByteString.Char8 ((<?>))
import Control.Applicative ((<*>), (*>), (<*), (<$>), (<|>), pure)
import qualified Data.ByteString.Char8 as B
import Data.Maybe
import Data.Char (isSpace)
import Control.Proxy.Attoparsec
import Control.Proxy.Attoparsec.Control
import Control.Proxy
import Control.Proxy.Trans.Either
import Types.CGI.Response
import Types.CGI.Headers

-- |Takes a 'ByteString' and parses a 'CGIResponse' from it.
parseCGIProxy :: (Proxy p, Monad m) => () -> Pipe (EitherP BadInput p) B.ByteString CGIResponse m r
parseCGIProxy = parserInputD >-> throwParsingErrors >-> parserD parseCGIResponse

parseCGIResponse :: Parser CGIResponse
parseCGIResponse = CGIResponse <$> parseCGIHeaders <*> A.takeTill ((==) '\EOT') <?> "parsecgiresp"

parseCGIHeaders :: Parser CGIHeaders
parseCGIHeaders = AC.manyTill parseCGIHeader (A.endOfLine) <?> "parseheaders"-- <* A.string (B.pack "\r\n")

parseCGIHeader :: Parser CGIHeader
parseCGIHeader = 
    (A.stringCI (B.pack "CONTENT-TYPE") *> ctype) <|>
    (A.stringCI (B.pack "LOCATION") *> location) <|>
    (A.stringCI (B.pack "STATUS") *> status) <?> "parsecgiheader"
    where
        ctype = ContentType <$> (skipColonAndSpace *> parseHeaderValue) <?> "content-type"
        location = Location <$> (skipColonAndSpace *> parseHeaderValue) <?> "location"
        status = Status <$> (skipColonAndSpace *> getDigit) <?> "location"
        getDigit = fst . fromJust . B.readInteger <$> A.takeWhile1 (A.inClass "0-9")

parseHeaderValue = B.concat <$> AC.manyTill (B.singleton <$> A.anyChar) (A.endOfLine) <?> "parseheadervalue"

skipColonAndSpace = A.skipWhile (== ':') *> A.skipWhile (\x -> x == ' ' || x == '\t')
