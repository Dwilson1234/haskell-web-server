-- |This module does all the parsing for HTTP 'Header' values.
module Parsers.HTTP.Headers (headersProxy, parseHeaders) where 

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.Attoparsec.Combinator as AC
import Data.Attoparsec.ByteString.Char8 ((<?>))
import Data.Attoparsec.ByteString.Char8 (Parser)
import Control.Applicative ((<*>), (*>), (<*), (<$>), (<|>), pure)
import qualified Data.ByteString.Char8 as B
import Data.Maybe
import Control.Proxy.Attoparsec
import Control.Proxy
import Types.HTTP.Headers

-- |Takes a 'ByteString' and parses it into HTTP 'Headers'.
headersProxy :: (Proxy p, Monad m) => () -> Pipe p B.ByteString Headers m r
headersProxy = parserInputD >-> parserD parseHeaders

-- |Parses a list of headers. Needed occasionally by 'HTTPRequest' and 'HTTPResponse' modules.
parseHeaders :: Parser Headers
parseHeaders = AC.manyTill parseHeader (A.endOfLine) <?> "parseheaders"-- <* A.string (B.pack "\r\n")

parseHeader :: Parser Header
parseHeader = AC.choice [parseReqHeader, parseGenHeader, parseEntHeader, parseCustomHeader] <?> "parseHeader"

parseReqHeader :: Parser Header
parseReqHeader = RequestHeader <$> doH <*> (skipColonAndSpace *> parseHeaderValue) <?> "parsereqheader"
    where
        doH = AC.choice [
            A.stringCI (B.pack "ACCEPT-CHARSET") *> pure AcceptCharset,
            A.stringCI (B.pack "ACCEPT-ENCODING") *> pure AcceptEncoding,
            A.stringCI (B.pack "ACCEPT-LANGUAGE") *> pure AcceptLanguage,
            A.stringCI (B.pack "ACCEPT") *> pure Accept,
            A.stringCI (B.pack "AUTHORIZATION") *> pure Authorization,
            A.stringCI (B.pack "EXPECT") *> pure Expect,
            A.stringCI (B.pack "FROM") *> pure From,
            A.stringCI (B.pack "HOST") *> pure Host,
            A.stringCI (B.pack "IF-MATCH") *> pure IfMatch,
            A.stringCI (B.pack "IF-MODIFIED-SINCE") *> pure IfModifiedSince,
            A.stringCI (B.pack "IF-NONE-MATCH") *> pure IfNoneMatch,
            A.stringCI (B.pack "IF-RANGE") *> pure IfRange,
            A.stringCI (B.pack "IF-UNMODIFIED-SINCE") *> pure IfUnmodifiedSince,
            A.stringCI (B.pack "MAX-FORWARDS") *> pure MaxForwards,
            A.stringCI (B.pack "PROXY-AUTHORIZATION") *> pure ProxyAuthorization,
            A.stringCI (B.pack "RANGE") *> pure Range,
            A.stringCI (B.pack "REFERER") *> pure Referer,
            A.stringCI (B.pack "TE") *> pure TE,
            A.stringCI (B.pack "USER-AGENT") *> pure UserAgent
            ]

parseGenHeader :: Parser Header
parseGenHeader = GeneralHeader <$> doH <*> (skipColonAndSpace *> parseHeaderValue) <?> "parsegenheader"
    where
        doH = AC.choice [
            A.stringCI (B.pack "cache-control") *> pure CacheControl,
            A.stringCI (B.pack "connection") *> pure Connection,
            A.stringCI (B.pack "date") *> pure Date,
            A.stringCI (B.pack "pragma") *> pure Pragma,
            A.stringCI (B.pack "trailer") *> pure Trailer,
            A.stringCI (B.pack "transfer-encoding") *> pure TransferEncoding,
            A.stringCI (B.pack "upgrade") *> pure TransferEncoding,
            A.stringCI (B.pack "via") *> pure Via,
            A.stringCI (B.pack "warning") *> pure Warning
            ]
parseEntHeader :: Parser Header
parseEntHeader = EntityHeader <$> doH <*> (skipColonAndSpace *> parseHeaderValue) <?> "parseentheader"
    where
        doH = AC.choice [
            A.stringCI (B.pack "allow") *> pure Allow,
            A.stringCI (B.pack "content-encoding") *> pure ContentEncoding,
            A.stringCI (B.pack "content-language") *> pure ContentLanguage,
            A.stringCI (B.pack "content-length") *> pure ContentLength,
            A.stringCI (B.pack "content-location") *> pure ContentLocation,
            A.stringCI (B.pack "content-md5") *> pure ContentMD5,
            A.stringCI (B.pack "content-range") *> pure ContentRange,
            A.stringCI (B.pack "content-type") *> pure ContentType,
            A.stringCI (B.pack "expires") *> pure Expires,
            A.stringCI (B.pack "last-modified") *> pure LastModified
            ]

parseCustomHeader :: Parser Header
parseCustomHeader = CustomHeader <$> (A.takeTill (== ':')) <*> (skipColonAndSpace *> parseHeaderValue) <?> "parsecustomheader"

parseHeaderValue = HValue <$> B.concat <$> AC.manyTill (B.singleton <$> A.anyChar) (A.endOfLine) <?> "parseheadervalue"

skipColonAndSpace = A.skipWhile (== ':') *> A.skipWhile (\x -> x == ' ' || x == '\t')
