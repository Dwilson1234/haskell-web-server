-- |This module contains all the parsers for parsing HTTP Requests
module Parsers.HTTP.Request (parseRequestProxy, parseMessageProxy) where 

import qualified Data.Attoparsec.ByteString.Char8 as A
import Data.Attoparsec.ByteString.Char8 (Parser)
import Data.Attoparsec.ByteString.Char8 ((<?>))
import Control.Applicative ((<*>), (*>), (<*), (<$>), (<|>), pure)
import qualified Data.ByteString.Char8 as B
import Data.Maybe
import Data.Char (isSpace)
import Control.Proxy.Attoparsec
import Control.Proxy.Attoparsec.Control
import Control.Proxy
import Control.Proxy.Trans.Either
import Types.HTTP.Request
import Types.HTTP.Misc
import Parsers.HTTP.Headers

-- |Takes a 'ByteString' and parses it into an 'HTTPRequest' with no 'Message' body. Parsers are not buffered, which is why
-- we need to read one byte at a time from the socket.
parseRequestProxy :: (Proxy p, Monad m) => () -> Pipe (EitherP BadInput p) B.ByteString HTTPRequest m r
parseRequestProxy = parserInputD >-> throwParsingErrors >-> parserD parseRequest

parseRequest :: Parser HTTPRequest
parseRequest = A.stringCI (B.pack "GET") *> get <|> 
               A.stringCI (B.pack "POST") *> post <?> "parserequest"--HTTPRequest <$> parseRequestLine <*> parseHeaders <*> pure (Message "message") <?> "ParseRequest"
    where
        get = GET <$> parseURIProt <*> parseHeaders
        post = POST <$> parseURIProt <*> parseHeaders <*> pure Nothing

-- |Takes a 'ByteString' and number of bytes to read and parses a 'Message' object. This eventually gets inserted into an 'HTTPRequest'.
parseMessageProxy :: (Proxy p, Monad m) => Int -> () -> Pipe (EitherP BadInput p) B.ByteString Message m r
parseMessageProxy n = parserInputD >-> throwParsingErrors >-> parserD (parseMessage n)

parseMessage :: Int -> Parser Message
parseMessage n = Message <$> (B.unpack <$> A.take n)

-- request line parsers and proxies
parseURIProt :: Parser URIProt
parseURIProt = URIProt <$> (A.skipWhile (isSpace) *> parseURI) <*> (A.skipWhile (isSpace) *> parseProtocol <* A.endOfLine) <?> "parserequestline"

parseProtocol :: Parser Protocol
parseProtocol = Protocol <$> (A.stringCI (B.pack "HTTP/") *> getDigit) <*> (A.char '.' *> getDigit) <?> "parseprotocol"
    where getDigit = fst . fromJust . B.readInteger <$> A.takeWhile1 (A.inClass "0-9")

parseURI :: Parser URI
parseURI = URI <$> A.takeWhile1 (not . isSpace) <?> "parseURI"

