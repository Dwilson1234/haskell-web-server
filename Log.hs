-- |This module contains logging functions
module Log (doLog) where

import Types.HTTP.Request
import Types.HTTP.Response
import Types.HTTP.Headers
import Types.HTTP.Misc
import Control.Proxy

-- does the actual log construction.
mkLog (req, resp) remoteAddr = 
    show remoteAddr
    ++ " - - " ++ " [" ++
    case date of
        Just d -> d
        Nothing -> "-"
    ++ "] " ++ rl ++ " " ++
    show (getStatusCode resp) ++ " " ++
    show (length m)
    where
        date = getDate $ getReqHeaders req
        rl = getRequestLine req
        (Message m) = getRespMessage resp

-- |Pipe that prints a log to the output. Depends on HTTP request and response for input.
doLog remoteAddr () = runIdentityP $ forever $ do
    (req, resp) <- request ()
    lift $ putStrLn $ mkLog (req, resp) remoteAddr
    respond resp
