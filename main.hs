module Main (main) where

import Control.Proxy
import Control.Proxy.TCP hiding (Host)
import qualified Control.Proxy.TCP as TCP (HostPreference(Host))
import Control.Proxy.Trans
import Control.Proxy.Trans.Either
import Control.Proxy.Safe
import Control.Exception.Base
import Types.HTTP.Request
import Types.HTTP.Response
import Types.HTTP.Misc
import Types.HTTP.Headers
import Types.CGI.Response
import Types.CGI.Environment
import qualified Types.CGI.Headers as CGI
import Types.Exceptions
import Parsers.HTTP.Request
import Parsers.HTTP.Headers
import Parsers.CGI.Response
import qualified Data.ByteString.Char8 as B
import Data.List
import Data.Either
import Data.Time.Clock
import Data.Time.Format
import System.Locale
import System.Directory
import System.Process
import System.Exit
import System.Posix.Env
import System.FilePath.Posix
import Config
import Log

main = do
    startupMessage
    serveFork (TCP.Host serverHost) serverPort $
        \(socket, remoteAddr) -> do
            ret <- runSafeIO $ runProxy $ runEitherK $ runEitherK $ palpha socket remoteAddr
            case ret of
                Left _ -> undefined
                Right _ -> return ()

-- print startup message
startupMessage :: IO ()
startupMessage = do
    putStrLn "Warming up web server..."
    putStrLn "\tCreated by Daniel Wilson"

-- I split a lot of functions off into smaller groups so that they are easier to read. This
-- is one example.

-- palpha is the pipeline
palpha socket remoteAddr = 
    mapP (pstart socket >-> pRP)
    >-> mapP (pM socket )
    >-> mapP (routeRequest >-> phGET >-> phPOST >-> pmerge) 
    >-> mapD (\(req, (m, hs, sc)) -> (req, HTTPResponse (Message m) hs (mkStatusLine sc))) >-> mapP (tryK (doLog remoteAddr)) 
    >-> mapP ((tryK (mapD (B.pack . show))) >-> pend socket)

-- socket pipes I read 1 byte at a time from the socket because it's not buffered
pstart socket = (tryK (socketReadS 1 socket))
pend socket = (tryK (socketWriteD socket))

-- parser Proxies, squashed so there's only one exception layer
pRP = (squashP . fmapL (SomeException . ParserException) . parseRequestProxy)
pMessage n = (squashP . fmapL (SomeException . ParserException) . parseMessageProxy n)

-- does some checking and then initiates the parser to parse the message body for httpreqs
pM socket () = EitherP $ runEitherP $ forever $ do
    req <- request ()
    let hs = getReqHeaders req
    let n = getN hs
    case n of
        Nothing -> respond req
        Just (n', _) -> ((pstart socket >-> pMessage (fromIntegral n') >-> mapD (insertMessage req)) <-< unitD) ()
    where
        getN ((EntityHeader ContentLength (HValue v)):_) = B.readInteger v
        getN (_:[]) = Nothing
        getN (_:hs) = getN hs

phGET = leftD handleGET

-- flatten cgi response parser into a single exeption layer
squishedCGIP = (squashP . fmapL (SomeException . ParserException) . parseCGIProxy)

-- hacks to parse the cgi response and forward it to the next pipe section
pCGI () = EitherP $ runEitherP $ do
    (req, str) <- request ()
    let bstr = B.pack str
    ((fromListS [bstr, B.singleton '\EOT'] >-> squishedCGIP >-> myreturn req) <-< unitD) ()
    where
        myreturn req () = runIdentityP $ forever $ do
            cgires <- request ()
            respond (req, cgires)

-- from gabriel gonzalez via email
slide :: Either a (Either b c) -> Either b (Either a c)
slide (Left         a ) = Right (Left  a)
slide (Right (Left  b)) = Left         b
slide (Right (Right c)) = Right (Right c)

-- pipe to handle POST requests
phPOST = rightD (handlePOST >-> rightD (pCGI) >-> mapD (either id cgi2httpparts))

-- merges GET and POST branches into a single pipe again
pmerge = mapD (either id id)

-- handles GET requests
handleGET () = do
    httpReq <- request ()
    let uri = (\(URI uri) -> B.unpack uri) $ getURI httpReq
    time <- tryIO $ getCurrentTime
    let timestr = formatTime defaultTimeLocale rfc822DateFormat time
    let path = if last uri == '/' then uri ++ "index.html" else uri
    ret <- tryIO $ trySafeIO $ runProxy $ runEitherK $ runWriterK $ (mapP (readFileS (documentRoot ++ path)) >-> foldD ("" ++))
    case ret of
        Left _ -> respond (httpReq, (notfound,
            [EntityHeader ContentType (HValue (B.pack "text/html")),
            EntityHeader ContentLength (HValue (B.pack (show (length notfound)))),
            GeneralHeader Date (HValue (B.pack timestr))],
            sc404))
        Right (_, file) -> respond (httpReq, (file,
            [EntityHeader ContentType (HValue (B.pack ("text/" ++ doType path))),
            EntityHeader ContentLength (HValue (B.pack (show (length file)))),
            GeneralHeader Date (HValue (B.pack timestr))],
            sc200))
    where
        doType path = tail . takeExtension $ path


-- handles POST requests
handlePOST () = do
    httpReq <- request ()
    let uri = (\(URI uri) -> B.unpack uri) $ getURI httpReq
    time <- tryIO $ getCurrentTime
    let timestr = formatTime defaultTimeLocale rfc822DateFormat time
    let path = if last uri == '/' then uri ++ "index.html" else uri
    exists <- tryIO $ doesFileExist (documentRoot ++ path)
    -- do environment variables here
    let envs = getEnvVs httpReq
    tryIO $ mapM_ (putEnv . show) envs
    if exists
        then do
            (pstatus, stdout, stderr) <- tryIO $ doCGI httpReq path
            case pstatus of
                ExitSuccess -> respond $ Right (httpReq, stdout)
                ExitFailure _ -> do 
                    respond $ Left (httpReq, (internalerror,
                        [EntityHeader ContentType (HValue (B.pack "text/html")),
                        EntityHeader ContentLength (HValue (B.pack (show (length notfound)))),
                        GeneralHeader Date (HValue (B.pack timestr))],
                        sc500))
        else
            respond $ Left (httpReq, (notfound,
                [EntityHeader ContentType (HValue (B.pack "text/html")),
                EntityHeader ContentLength (HValue (B.pack (show (length notfound)))),
                GeneralHeader Date (HValue (B.pack timestr))],
                sc404))

-- does the spawning of the cgi process, blocks until it returns
doCGI req path = do
    let (Message m) = getReqMessage req
    let exe = if isSuffixOf "php" path then "php5" else "perl"
    pinfo <- readProcessWithExitCode exe [path] m
    return pinfo

-- routes a request to GET or POST branches depending on the method
routeRequest ::
    (Monad m, Proxy p)
    => () -> Pipe p HTTPRequest (Either HTTPRequest HTTPRequest) m r
routeRequest () = runIdentityP $ forever $ do
    httpReq <- request ()
    respond $ case httpReq of
      (GET _ _) -> Left  httpReq
      (POST _ _ _) -> Right httpReq

